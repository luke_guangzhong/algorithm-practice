/**
 * @file Solution.h
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2024-05-06
 *
 * @copyright Copyright (c) 2024
 *
 */
#ifndef SOLUTION_H
#define SOLUTION_H

#include <vector>

class Solution
{
  public:
    std::vector<int> twoSum(std::vector<int>& nums, int target);
};

#endif // SOLUTION_H
/**
 * @file main.cpp
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2024-05-06
 *
 * @copyright Copyright (c) 2024
 *
 */

#include "Solution.h"
#include <iostream>
#include <vector>

int
main()
{
    std::vector<int> v = {2, 7, 11, 15};
    std::vector<int> r = {};
    Solution         s;

    r = s.twoSum(v, 9);

    if (r.size() == 2) {
        std::cout << "[ " << r[0] << " " << r[1] << " ]" << std::endl;
    }

    return EXIT_SUCCESS;
}